import React from 'react';
import ReactDOM from 'react-dom/client';
import {BrowserRouter, Routes, Route} from 'react-router-dom';
import {Provider} from 'react-redux';
import {store} from './app/store';

import App from './components/App';
import Cars from './components/Cars';

import './components/App.css';

const root = ReactDOM.createRoot(document.querySelector('#root'));

root.render(
  <React.StrictMode>
    <Provider store={store}>
      <BrowserRouter>
        <Routes>
          <Route path='/' element={<App />} />
          <Route path="/cars" element={<Cars />} />
        </Routes>
      </BrowserRouter>
    </Provider>
  </React.StrictMode>
);