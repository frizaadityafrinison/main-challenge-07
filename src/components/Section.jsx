import React from "react";

function Section() {
  return (
    <div className="section">
      <div className="container">
        <div className="row our-services" id="our-services">
          <div className="col-lg-6 col-sm-12 our-services-images">
            <img src="images/maskgroup-cropped.png" alt="" />
          </div>
          <div className="col-lg-6 col-sm-12 our-services-text">
            <p className="hero2-text">
              Best Car Rental for any kind of trip in (Lokasimu)!
            </p>
            <p className="hero3-text">
              Sewa mobil di (Lokasimu) bersama Binar Car Rental jaminan harga
              lebih murah dibandingkan yang lain, kondisi mobil baru, serta
              kualitas pelayanan terbaik untuk perjalanan wisata, bisnis,
              wedding, meeting, dll.
            </p>
            <div className="marklist-container col-sm-12">
              <div className="marklist">
                <img src="images/checklist.png" alt="" />
                <p className="hero3-text">
                  Sewa Mobil Dengan Supir di Bali 12 Jam
                </p>
              </div>
              <div className="marklist">
                <img src="images/checklist.png" alt="" />
                <p className="hero3-text">
                  Sewa Mobil Lepas Kunci di Bali 12 Jam
                </p>
              </div>
              <div className="marklist">
                <img src="images/checklist.png" alt="" />
                <p className="hero3-text">Sewa Mobil Jangka Panjang Bulanan</p>
              </div>
              <div className="marklist">
                <img src="images/checklist.png" alt="" />
                <p className="hero3-text">
                  Gratis Antar - Jemput Mobil di Bandara
                </p>
              </div>
              <div className="marklist">
                <img src="images/checklist.png" alt="" />
                <p className="hero3-text">
                  Layanan Airport Transfer / Drop In Out
                </p>
              </div>
            </div>
          </div>
        </div>
      </div>

      <div className="container why-us-custom" id="why-us">
        <div className="row">
          <div className="col-lg-12 col-sm-12 p-0 why-us-header">
            <p className="hero2-text">Why Us?</p>
            <p className="hero3-text">Mengapa harus pilih Binar Car Rental?</p>
          </div>
        </div>
        <div className="row body-card-container">
          <div className="col-lg-3 col-sm-12 body-card">
            <div className="social-media-icon">
              <img src="images/top.png" alt="" />
              <p className="hero4-text">Mobil Lengkap</p>
              <p className="hero3-text">
                Tersedia banyak pilihan mobil, kondisi masih baru, bersih dan
                terawat
              </p>
            </div>
          </div>
          <div className="col-lg-3 col-sm-12 body-card">
            <div className="social-media-icon">
              <img src="images/tag.png" alt="" />
              <p className="hero4-text">Harga Murah</p>
              <p className="hero3-text">
                Harga murah dan bersaing, bisa bandingkan harga kami dengan
                rental mobil lain
              </p>
            </div>
          </div>
          <div className="col-lg-3 col-sm-12 body-card">
            <div className="social-media-icon">
              <img src="images/time.png" alt="" />
              <p className="hero4-text">Layanan 24 Jam</p>
              <p className="hero3-text">
                Siap melayani kebutuhan Anda selama 24 jam nonstop. Kami juga
                tersedia di akhir minggu
              </p>
            </div>
          </div>
          <div className="col-lg-3 col-sm-12 body-card">
            <div className="social-media-icon">
              <img src="images/medal.png" alt="" />
              <p className="hero4-text">Sopir Profesional</p>
              <p className="hero3-text">
                Sopir yang profesional, berpengalaman, jujur, ramah dan selalu
                tepat waktu
              </p>
            </div>
          </div>
        </div>
      </div>

      <div className="container" id="testimonial">
        <div className="row testimonial-custom">
          <div className="col-lg-12 col-sm-12 p-0 text-center">
            <p className="hero2-text" style={{ width: "100%" }}>
              Testimonial
            </p>
            <p className="hero3-text" style={{ width: "100%" }}>
              Berbagai review positif dari pelanggan kami
            </p>
          </div>
        </div>
        <div className="row testimonial-container">
          <div
            id="carouselExampleControls"
            className="col-lg-12 col-sm-12 carousel-container carousel slide"
            data-bs-ride="carousel"
          >
            <div className="carousel-inner ">
              <div className="carousel-item active">
                <div className="banner-body">
                  <div className="banner-body-text">
                    <div className="banner-image">
                      <img src="images/male.png" alt="" srcSet="" />
                    </div>
                    <div className="banner-text">
                      <div className="star-review">
                        <i className="fa-solid fa-star"></i>
                        <i className="fa-solid fa-star"></i>
                        <i className="fa-solid fa-star"></i>
                        <i className="fa-solid fa-star"></i>
                        <i className="fa-solid fa-star"></i>
                      </div>
                      <div className="review">
                        <p className="hero3-text">
                          "Lorem ipsum dolor sit amet consectetur adipisicing
                          elit. Inventore laudantium placeat dolore nesciunt
                          ipsum nulla vero sunt illo ea ullam aliquam recusandae
                          voluptate alias, facilis praesentium molestias esse
                          tempore. Odit."
                        </p>
                      </div>
                      <div className="reviewer">
                        <p className="hero3-text">John Dee 32, Bromo</p>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div className="carousel-item">
                <div className="banner-body">
                  <div className="banner-body-text">
                    <div className="banner-image">
                      <img src="images/female.png" alt="" srcSet="" />
                    </div>
                    <div className="banner-text">
                      <div className="star-review">
                        <i className="fa-solid fa-star"></i>
                        <i className="fa-solid fa-star"></i>
                        <i className="fa-solid fa-star"></i>
                        <i className="fa-solid fa-star"></i>
                        <i className="fa-solid fa-star"></i>
                      </div>
                      <div className="review">
                        <p className="hero3-text">
                          "Lorem ipsum dolor sit amet consectetur adipisicing
                          elit. Inventore laudantium placeat dolore nesciunt
                          ipsum nulla vero sunt illo ea ullam aliquam recusandae
                          voluptate alias, facilis praesentium molestias esse
                          tempore. Odit."
                        </p>
                      </div>
                      <div className="reviewer">
                        <p className="hero3-text">John Dee 32, Bromo</p>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div className="carousel-button">
              <a
                href=""
                data-bs-target="#carouselExampleControls"
                data-bs-slide="prev"
              >
                <img
                  src="images/previous.png"
                  className="carousel-button"
                  alt=""
                  srcSet=""
                />
              </a>
              <a
                href=""
                data-bs-target="#carouselExampleControls"
                data-bs-slide="next"
              >
                <img
                  src="images/next.png"
                  className="carousel-button"
                  alt=""
                  srcSet=""
                />
              </a>
            </div>
          </div>
        </div>
      </div>

      <div className="container">
        <div className="row row-container">
          <div className="col-lg-12 col-sm-12 banner-body">
            <div className="banner-body-text">
              <p className="hero1-text">Sewa Mobil di (Lokasimu) Sekarang</p>
              <p className="hero3-text">
                Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
                eiusmod tempor incididunt ut labore et dolore magna aliqua.{" "}
              </p>
              <button
                className="nav-item nav-link-custom btn button-custom"
                href="#"
                style={{ color: "#FFFFFF !important", marginTop: "45px" }}
              >
                Mulai Sewa Mobil
              </button>
            </div>
          </div>
        </div>
      </div>

      <div className="accordion faq" id="accordionExample">
        <div className="container" id="faq">
          <div className="row">
            <div className="col-lg-6 col-sm-12 faq-header">
              <p className="hero1-text">Frequently Asked Question</p>
              <p className="hero3-text" style={{marginBottom: "24px"}}>
                Lorem ipsum dolor sit amet, consectetur adipiscing
              </p>
            </div>
            <div className="col-lg-6 col-sm-12">
              <div className="accordion" id="accordionExample">
                <div className="accordion-item">
                  <h2 className="accordion-header" id="headingOne">
                    <button
                      className="accordion-button collapsed"
                      type="button"
                      data-bs-toggle="collapse"
                      data-bs-target="#collapseOne"
                      aria-expanded="true"
                      aria-controls="collapseOne"
                    >
                      Apa saja syarat yang dibutuhkan?
                    </button>
                  </h2>
                  <div
                    id="collapseOne"
                    className="accordion-collapse collapse"
                    aria-labelledby="headingOne"
                    data-bs-parent="#accordionExample"
                  >
                    <div className="accordion-body">
                      <p>
                        Lorem ipsum dolor sit amet consectetur adipisicing elit.
                        Ut quam, repellat sapiente eum voluptatum porro enim
                        nulla ipsa dolore eveniet nostrum reprehenderit
                        consectetur ea sint, quae doloremque excepturi est
                        consequuntur?
                      </p>
                    </div>
                  </div>
                </div>
                <div className="accordion-item">
                  <h2 className="accordion-header" id="headingTwo">
                    <button
                      className="accordion-button collapsed"
                      type="button"
                      data-bs-toggle="collapse"
                      data-bs-target="#collapseTwo"
                      aria-expanded="false"
                      aria-controls="collapseTwo"
                    >
                      Berapa hari minimal sewa mobil lepas kunci?
                    </button>
                  </h2>
                  <div
                    id="collapseTwo"
                    className="accordion-collapse collapse"
                    aria-labelledby="headingTwo"
                    data-bs-parent="#accordionExample"
                  >
                    <div className="accordion-body">
                      <p>
                        Lorem ipsum dolor sit amet consectetur adipisicing elit.
                        Ut quam, repellat sapiente eum voluptatum porro enim
                        nulla ipsa dolore eveniet nostrum reprehenderit
                        consectetur ea sint, quae doloremque excepturi est
                        consequuntur?
                      </p>
                    </div>
                  </div>
                </div>
                <div className="accordion-item">
                  <h2 className="accordion-header" id="headingThree">
                    <button
                      className="accordion-button collapsed"
                      type="button"
                      data-bs-toggle="collapse"
                      data-bs-target="#collapseThree"
                      aria-expanded="false"
                      aria-controls="collapseThree"
                    >
                      Berapa hari sebelumnya sabaiknya booking sewa mobil?
                    </button>
                  </h2>
                  <div
                    id="collapseThree"
                    className="accordion-collapse collapse"
                    aria-labelledby="headingThree"
                    data-bs-parent="#accordionExample"
                  >
                    <div className="accordion-body">
                      <p>
                        Lorem ipsum dolor sit amet consectetur adipisicing elit.
                        Ut quam, repellat sapiente eum voluptatum porro enim
                        nulla ipsa dolore eveniet nostrum reprehenderit
                        consectetur ea sint, quae doloremque excepturi est
                        consequuntur?
                      </p>
                    </div>
                  </div>
                </div>
                <div className="accordion-item">
                  <h2 className="accordion-header" id="headingThree">
                    <button
                      className="accordion-button collapsed"
                      type="button"
                      data-bs-toggle="collapse"
                      data-bs-target="#collapseFourth"
                      aria-expanded="false"
                      aria-controls="collapseFourth"
                    >
                      Apakah Ada biaya antar-jemput?
                    </button>
                  </h2>
                  <div
                    id="collapseFourth"
                    className="accordion-collapse collapse"
                    aria-labelledby="headingFourth"
                    data-bs-parent="#accordionExample"
                  >
                    <div className="accordion-body">
                      <p>
                        Lorem ipsum dolor sit amet consectetur adipisicing elit.
                        Ut quam, repellat sapiente eum voluptatum porro enim
                        nulla ipsa dolore eveniet nostrum reprehenderit
                        consectetur ea sint, quae doloremque excepturi est
                        consequuntur?
                      </p>
                    </div>
                  </div>
                </div>
                <div className="accordion-item">
                  <h2 className="accordion-header" id="headingThree">
                    <button
                      className="accordion-button collapsed"
                      type="button"
                      data-bs-toggle="collapse"
                      data-bs-target="#collapseFifth"
                      aria-expanded="false"
                      aria-controls="collapseFifth"
                    >
                      Bagaimana jika terjadi kecelakaan
                    </button>
                  </h2>
                  <div
                    id="collapseFifth"
                    className="accordion-collapse collapse"
                    aria-labelledby="headingFifth"
                    data-bs-parent="#accordionExample"
                  >
                    <div className="accordion-body">
                      <p>
                        Lorem ipsum dolor sit amet consectetur adipisicing elit.
                        Ut quam, repellat sapiente eum voluptatum porro enim
                        nulla ipsa dolore eveniet nostrum reprehenderit
                        consectetur ea sint, quae doloremque excepturi est
                        consequuntur?
                      </p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      
    </div>
  );
}

export default Section;
