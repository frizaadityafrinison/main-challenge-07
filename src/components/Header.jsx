import React from "react";
import { NavLink } from "react-router-dom";

function Header() {
  return (
    <div>
      <nav
        className="navbar fixed-top navbar-expand-lg navbar-light"
        style={{ height: "84px", backgroundColor: "#F1F3FF" }}
      >
        <div className="container">
          <div className="logo"></div>
          <div className="offcanvas-custom">
            <button
              className="btn"
              type="button"
              data-bs-toggle="offcanvas"
              data-bs-target="#offcanvasRight"
              aria-controls="offcanvasRight"
            >
              <span className="navbar-toggler-icon"></span>
            </button>
            <div
              className="offcanvas offcanvas-end offcanvas-end-custom"
              tabIndex="-1"
              id="offcanvasRight"
              aria-labelledby="offcanvasRightLabel"
            >
              <div className="offcanvas-header offcanvas-header-custom">
                <span style={{ fontWeight: "700" }}>BCR</span>
                <button
                  type="button"
                  className="btn-close text-reset"
                  data-bs-dismiss="offcanvas"
                  aria-label="Close"
                ></button>
              </div>
              <div className="offcanvas-body offcanvas-body-custom">
                <div className="navbar-nav ml-auto ">
                  <a
                    className="nav-link nav-link-custom"
                    aria-current="page"
                    href="#our-services"
                  >
                    Our Services
                  </a>
                  <a className="nav-link nav-link-custom" href="#why-us">
                    Why Us
                  </a>
                  <a className="nav-link nav-link-custom" href="#testimonial">
                    Testimonial
                  </a>
                  <a className="nav-link nav-link-custom" href="#faq">
                    FAQ
                  </a>
                  <button
                    className="nav-item nav-link-custom mt-2 btn button-custom"
                    href="#"
                    style={{ color: "#FFFFFF !important", width: "60%" }}
                  >
                    Register
                  </button>
                </div>
              </div>
            </div>
          </div>
          <div className="navbar-nav ml-auto navbar-nav-custom">
            <a
              className="nav-link nav-link-custom"
              aria-current="page"
              href="#our-services"
            >
              Our Services
            </a>
            <a className="nav-link nav-link-custom" href="#why-us">
              Why Us
            </a>
            <a className="nav-link nav-link-custom" href="#testimonial">
              Testimonial
            </a>
            <a className="nav-link nav-link-custom" href="#faq">
              FAQ
            </a>
            <button
              className="nav-item nav-link-custom btn button-custom"
              href="#"
              style={{ color: "#FFFFFF !important" }}
            >
              Register
            </button>
          </div>
        </div>
      </nav>

      <div className="jumbotron jumbotron-custom">
        <div className="container">
          <div className="row jumbotron-row">
            <div className="col-lg-6 col-sm-12 jumbotron-text">
              <div className="text">
                <span className="hero1-text">
                  Sewa & Rental Mobil Terbaik di kawasan (Lokasimu)
                </span>
                <p className="hero3-text" style={{ marginTop: "16px" }}>
                  Selamat datang di Binar Car Rental. Kami menyediakan mobil
                  kualits terbaik dengan harga terjangkau. Selalu siap melayani
                  kebutuhanmu untuk sewa mobil selama 24 jam.
                </p>
                <NavLink
                  className="nav-item nav-link-custom btn button-custom"
                  to="/cars"
                  style={{ color: "#FFFFFF !important", marginTop: "16px" }}
                >
                  Mulai Sewa Mobil
                </NavLink>
              </div>
            </div>
            <div className="col-lg-6 col-sm-12 jumbotron-image">
              <div className="img">
                <img src="images/cars.png" alt="" />
              </div>
            </div>
            <div className="bg"></div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default Header;
