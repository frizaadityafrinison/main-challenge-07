import React, { useState, useEffect } from "react";
import { useSelector, useDispatch } from "react-redux";
import { getAllCars, fetchCars } from "../features/cars/carsSlice";

function Search() {
  const dispatch = useDispatch();
  const cars = useSelector(getAllCars);
  const carsStatus = useSelector((state) => state.cars.status);
  const error = useSelector((state) => state.cars.error);

  const [filteredData, setFilteredData] = useState([]);
  const [capacity, setCapacity] = useState();
  const [tanggal, setTanggal] = useState();
  const [waktuJemput, setWaktuJemput] = useState();

  useEffect(() => {
    if (carsStatus === "idle") {
      dispatch(fetchCars());
    }
  }, [carsStatus, dispatch]);

  function handleButtonClick() {
    const filteredCar = cars.filter((item) => {
      const inputTime = new Date(`${tanggal} ${waktuJemput}`);
      const miliTimeInput = inputTime.getTime();

      const dataTime = new Date(item.availableAt);
      const miliDataTime = Number(dataTime.getTime());

      const capacityFilter = item.capacity >= capacity;
      const dateFilter = miliDataTime < miliTimeInput;

      return capacityFilter && dateFilter;
    });

    setFilteredData(filteredCar);
  }

  function handleClickReset() {
    setFilteredData([]);
  }

  function handleChangeCapacity(event) {
    setCapacity(event.target.value);
  }

  function handleChangeDate(event) {
    setTanggal(event.target.value);
  }

  function handleChangeTime(event) {
    setWaktuJemput(event.target.value);
  }

  let content;

  if (carsStatus === "loading") {
    content = <div className="">Loading...</div>;
  } else if (carsStatus === "succeeded") {
    content = filteredData.map(function (item) {
      return (
        <div key={item.id} className="mobil-pencarian col-lg-4 col-sm-12">
          <div className="card-pencarian-body">
            <div className="pencarian-body-image">
              <img
                src={item.image}
                alt={item.manufacture}
                className="car-thumbnail"
              />
            </div>
            <div className="pencarian-body-text">
              <p className="tipe-mobil">
                {item.manufacture} {item.model} / {item.type}
              </p>
              <p className="harga-mobil">
                Rp{" "}
                {item.rentPerDay
                  .toString()
                  .replace(/\B(?=(\d{3})+(?!\d))/g, ".")}{" "}
                / hari
              </p>
              <p className="deskripsi-mobil">{item.description}</p>
              <div className="col-lg-6 col-sm-12 our-services-text">
                <div className="marklist-container col-sm-12">
                  <div className="marklist">
                    <img
                      src="images/people.png"
                      style={{ width: "20px" }}
                      alt=""
                    />
                    <p className="hero3-text jumlah-kursi">
                      {item.capacity} orang
                    </p>
                  </div>
                  <div className="marklist">
                    <img
                      src="images/gear.png"
                      style={{ width: "20px" }}
                      alt=""
                    />
                    <p className="hero3-text jumlah-kursi">
                      {item.transmission}
                    </p>
                  </div>
                  <div className="marklist">
                    <img
                      src="images/date.png"
                      style={{ width: "20px" }}
                      alt=""
                    />
                    <p className="hero3-text jumlah-kursi">{item.year}</p>
                  </div>
                </div>
              </div>
              <button
                className="nav-item nav-link-custom btn button-custom"
                href="#"
                style={{
                  color: "#FFFFFF !important",
                  marginTop: "24px",
                  width: "100%",
                }}
              >
                Pilih Mobil
              </button>
            </div>
          </div>
        </div>
      );
    });
  } else if (carsStatus === "failed") {
    content = <div className="">{error}</div>;
  }

  return (
    <section>
      <div className="form-pencarian">
        <div className="container">
          <div className="row form g-3">
            <div className="col-lg-3 col-sm-12">
              <label htmlFor="tipe-driver" className="form-label">
                Tipe Driver
              </label>
              <select className="form-select" id="tipe-driver" required>
                <option selected disabled value="" hidden>
                  Pilih Tipe Driver
                </option>
                <option>Dengan Sopir</option>
                <option>Tanpa Sopir (Lepas Kunci)</option>
              </select>
            </div>

            <div className="col-lg-3 col-sm-12">
              <label htmlFor="tanggal" className="form-label">
                Tanggal
              </label>
              <input
                type="date"
                onChange={handleChangeDate}
                className="form-control"
                id="tanggal"
                required
              />
            </div>

            <div className="col-lg-3 col-sm-12">
              <label htmlFor="waktu-jemput" className="form-label">
                Waktu Jemput/Ambil
              </label>
              <input
                type="time"
                onChange={handleChangeTime}
                className="form-control"
                id="waktu-jemput"
                required
              />
            </div>

            <div className="col-lg-3 col-sm-12">
              <label htmlFor="jumlah-penumpang" className="form-label">
                Jumlah Penumpang (Optional)
              </label>
              <input
                type="number"
                onChange={handleChangeCapacity}
                placeholder="Jumlah Penumpang"
                className="form-control"
                id="jumlah-penumpang"
                required
              />
            </div>
          </div>
          <div className="search-button">
            <button
              type="submit"
              id="load-btn"
              className="btn asd"
              onClick={handleButtonClick}
            >
              <i className="fa-solid fa-magnifying-glass"></i>
            </button>
            <button
              type="reset"
              id="clear-btn"
              className="btn asd"
              onClick={handleClickReset}
            >
              <i className="fa-solid fa-delete-left"></i>
            </button>
          </div>
        </div>
      </div>

      <div className="hasil-pencarian">
        <div className="container">
          <div id="cars-container" className="row card-pencarian">
            {content}
          </div>
        </div>
      </div>
    </section>
  );
}

export default Search;
