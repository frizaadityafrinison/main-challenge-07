import React from "react";

function Footer() {
    return(
        <footer>
            <div className="container">
                <div className="row footer-custom">
                <div className="col-lg-3 col-sm-12">
                    <p className="hero3-text">Jalan Suroyo No. 161 Mayangan Kota Probolonggo 672000</p>
                    <p className="hero3-text">binarcarrental@gmail.com</p>
                    <p className="hero3-text">081-233-334-808</p>
                </div>
                <div className="col-lg-2 col-sm-12">
                    <a className="nav-link nav-link-custom" id="#our-services" aria-current="page" href="#">Our Services</a>
                    <a className="nav-link nav-link-custom" href="#">Why Us</a>
                    <a className="nav-link nav-link-custom" href="#">Testimonial</a>
                    <a className="nav-link nav-link-custom">FAQ</a>
                </div>
                <div className="col-lg-4 col-sm-12">
                    <p className="hero3-text">Connect with us</p>
                    <div className="social-media-icon">
                    <a href="#">
                        <img src="images/facebook.png" alt="" srcSet="" />
                    </a>
                    <a href="#">
                        <img src="images/instagram.png" alt="" srcSet="" />
                    </a>
                    <a href="#">
                        <img src="images/twitch.png" alt="" srcSet="" />
                    </a>
                    <a href="#">
                        <img src="images/email.png" alt="" srcSet="" />
                    </a>
                    <a href="#">
                        <img src="images/twitch.png" alt="" srcSet="" />
                    </a>
                    </div>
                </div>
                <div className="col-lg-2 col-sm-12">
                    <p className="hero3-text">Copyright Binar 2022</p>
                    <div className="logo"></div>
                </div>
                </div>
            </div>
        </footer>
    );
}

export default Footer;