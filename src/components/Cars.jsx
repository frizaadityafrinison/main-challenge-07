import React from "react";

import Header from "./Header";
import Search from "./Search";
import Footer from "./Footer";


function Cars() {
    return(
        <div>
            <Header />
            <Search />
            <Footer />
        </div>
    ); 
}

export default Cars;