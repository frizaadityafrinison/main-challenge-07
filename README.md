# Binar Car With React® (Chapter 7 - Main Challenge)

A simple project made with love® and determination™, the project itself is the successor of project in Chapter 4, this project was bootstrapped with Create React App, using the Redux and Redux Toolkit template.

## Tech Stack
  * Node.js 16.14 (Runtime Environment)
  * Express 4.17.3 (HTTP Server)
  * Nodemon 2.0.16 (Server Runner)
  * ReactJS 18.1.0 (UI Library)
  * React Redux 8.0.2 (State Management)

## Prerequisite

These are things that you need to install on your machine before proceed to installation
* [Node.js](https://nodejs.org/)
* [NPM (Package Manager)](https://www.npmjs.com/)

## Installation

Clone this repository

```bash
cd desired_directory/
git clone https://gitlab.com/frizaadityafrinison/main-challenge-07
cd main-challenge-07
```

Download all the package and it's dependencies
```bash
npm install 
```

## Run The Server

Run the following command to start the server

```bash
  npm run start
```

Open [http://localhost:3000](http://localhost:3000) to view it in your browser.

The page will reload when you make changes. You may also see any lint errors in the console.

## Screenshot
![_777CCE27-13B2-4CD8-ABBE-1A669B6C0EC6_](/uploads/75076ae8c728f591777eccc4fdf2b360/_777CCE27-13B2-4CD8-ABBE-1A669B6C0EC6_.png)

![_7D979526-554A-4005-B871-9FC6310F5520_](/uploads/f1d8992549049a04b8d8db32f16aec8c/_7D979526-554A-4005-B871-9FC6310F5520_.png)

![_4BEC0E33-6424-4DE9-A17A-874281DBCD9F_](/uploads/36c9f96b05a4fb80b352d5e138c4acc0/_4BEC0E33-6424-4DE9-A17A-874281DBCD9F_.png)

![_75A5B16E-6548-4984-91EF-6B951E6194FC_](/uploads/bd82632183e97d59e4112782098f05b0/_75A5B16E-6548-4984-91EF-6B951E6194FC_.png)
